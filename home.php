<!DOCTYPE html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title><?= $page->title; ?></title>
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates?>styles/main.css" />
</head>
<body>

<?php

/* ------------- 
	Parcours des tables 'resource', 'property' et 'value' d'une db Omeka
	pour créer des tableaux comportant les class :
	auteurs, éditeurs, et livres.
----------------- */

/*
Technique :
	- les fonctions sont déclarées dans _functions.php (prepend spécifié dans config.php)	
	- utilisation de Tracy Debugger pour le debug, d'où les : bd($var, "Nom de variable");
	- la connexion à la base Omeka se fait par la class OmekaItems
	- la class OmekaItems construit des WireArray (tableaux spécifiques à ProcessWire) selon des filtres de class d'items ou non
	- une fois que les WireArray sont construits, il est possible d'utiliser l'API ProcessWire pour faire des output. Et notamment la méthode (find())[https://processwire.com/api/ref/pages/find/] accompagnée de (selectors)[https://processwire.com/docs/selectors/] pour filtrer les résultats

Class OmekaItems :
	- permet de se connecter à une base omeka (les variables sont à modifier)
	- buildClassArray() sert à construire un tableau de données, il est filtrable selon la class souhaitée. On peut avoir la totalité des items la base comme seulement les items de class "book". On aboutit à un tableau d'id qu'il faut passer dans buildResourceArray()
	- buildResourceArray() sert à retrouver la valeur d'une propriété. Lorsqu'elle n'est pas un litteral mais une ressource, il y a une exception pour aller retrouver le title de la ressource en question

Déroulé :
	- connexion à une base données omeka
	- création des valeurs pour books, authors et publishers avec la méthodes buildClassArray()
	- output de ces tableaux
	- filtrage de ces tableaux avec la méthode find() (API de ProcessWire)

Tables d'une base de données Omeka :
	- 'resource' : l'ensemble des items de la base
	avec la colonne 'resource_template_id' (ou 'resource_class_id'),
	on peut savoir de quel class est l'item
	- 'property' : l'ensemble des propriétés de l'ensemble des vocabulaires installés dans la base
	- 'value' : l'ensemble des valeurs ajoutées dans les champs des items, on peut retrouver
	l'item référent et l'id du champ / propriété

TODO
	- privilégier la propriété "dc:author" pour récupérer l'entièreté des auteurs
	- régler les choses quand plusieurs valeur sont renseignées pour une même propriété
	- convertir le tout en un module pour pouvoir l'appeler à la volée dans les templates
	- créer des fonctions de tri, de filtre
	- créer des fonctions pour récupérer les ressources où une ressource est liée
	- créer des fonctions pour filtrer les code MARC Relators et créer un  hyperlien vers le maker s'il existe dans la base

*/

?>


<?php

// instanciation de la class OmekaItems
$items = new OmekaItems;

// tableau des ids des class qui nous intéressent
// en fait ce sont les id des templates…
$class_array = [
	"author" => "2",
	"book" => "3",
	"publisher" => "4"
];

// création des listes pour les livres, les auteurs et les éditeurs
// variables : $table, $col_val, $col_label, $filter, $order, $dir
$items->books = $items->getByClass(3);
$items->authors = $items->getByClass(2);
$items->publishers = $items->getByClass(4);

// print_r($items->publishers);

?>


<h1>Listes</h1>
<?php // output des items des class en trois liste ?>

<h2>Les livres</h2>
<ul>
<?php foreach ($items->books as $key => $book): ?>
	<li><?= $book->title; ?></li>
<?php endforeach; ?>
</ul>

<h2>Les auteurs</h2>
<ul>
<?php foreach ($items->authors as $key => $author): ?>
	<li><?= $author->title; ?></li>
<?php endforeach; ?>
</ul>

<h2>Les éditeurs</h2>
<ul>
<?php foreach ($items->publishers as $key => $publisher): ?>
	<li><?= $publisher->title; ?></li>
<?php endforeach; ?>
</ul>

<hr>

<h1>Des résultats filtrés</h1>

<?php // avec les filtres des méthodes de processwire ?>

<h2>Les livres dont la date est inférieur à 1990</h2>
<pre>$items->books->find("date<1990")</pre>
<ul>
<?php foreach ($items->books->find("date<1990") as $key => $book): ?>
	<li><?= $book->title; ?></li>
<?php endforeach; ?>
</ul>

<h2>Les livres dont l'éditeur est "Éditions Allia"</h2>
<pre>$items->books->find("publisher=Éditions Allia")</pre>
<ul>
<?php foreach ($items->books->find("publisher=Éditions Allia") as $key => $book): ?>
	<li><?= $book->title; ?></li>
<?php endforeach; ?>
</ul>

<h2>Les éditeurs qui ne sont pas à Paris</h2>
<pre>$items->publishers->find("spatial!=Paris")</pre>
<ul>
<?php foreach ($items->publishers->find("spatial!=Paris") as $key => $publisher): ?>
	<li><?= $publisher->title; ?></li>
<?php endforeach; ?>
</ul>

<hr>

<?php // utilisation de la méthode get() pour récupérer des items spécifiques ?>

<h1>Retrouver le titre d'un item par son id</h1>

<pre>$items->getById(3);</pre>

<?php $book_id_3 = $items->getById(3); ?>

<dl>
    <dt>Titre</dt>
    <dd><?= $book_id_3->title ?></dd>

    <dt>Auteur</dt>
    <dd><?= $book_id_3->creator ?></dd>

    <dt>Éditeur</dt>
    <dd><?= $book_id_3->publisher ?></dd>

    <dt>Date</dt>
    <dd><?= $book_id_3->date ?></dd>
</dl>

<h1>Retrouver le titre d'un item par son titre</h1>

<?php $sullivan = $items->getByTitle("Sullivan, Louis"); ?>

<pre>$items->getByTitle("Sullivan, Louis"); </pre>

<dl>
    <dt>Prénom</dt>
    <dd><?= $sullivan->firstName ?></dd>

    <dt>Nom de famille</dt>
    <dd><?= $sullivan->familyName ?></dd>

    <dt>Date</dt>
    <dd><?= $sullivan->date ?></dd>
</dl>

</body>
</html>
