# ProcessWire Profile lié à Omeka

Contenu d'un dossier ```site/templates/``` d'une installation ProcessWire qui pioche des données dans une base de données Omeka.

## Installation

- Placer les fichiers dans le dossier ```site/templates/``` d'une installation ProcessWire.

> Pour appeler ```_functions.php``` avant les templates, bien penser à ajouter cette ligne dans ```site/config.php``` :

> ```$config->prependTemplateFile = '_functions.php';```

- Dans la méthode ```__construct``` de ```_functions.php```, modifier les informations de connexion à la base de données Omeka.
- Afficher la page d'accueil

## Technique

- les fonctions sont déclarées dans ```_functions.php``` (prepend spécifié dans config.php)	
- utilisation de Tracy Debugger pour le debug, d'où les : ```bd($var, "Nom de variable");```
- la connexion à la base Omeka se fait par la class OmekaItems
- la class OmekaItems construit des WireArray (tableaux spécifiques à ProcessWire) selon des filtres de class d'items ou non
- une fois que les WireArray sont construits, il est possible d'utiliser l'API ProcessWire pour faire des output. Et notamment la méthode [find()](https://processwire.com/api/ref/pages/find/) accompagnée de [selectors](https://processwire.com/docs/selectors/) pour filtrer les résultats

## Déroulé 

- connexion à une base données omeka
- création des valeurs pour books, authors et publishers avec la méthodes buildClassArray()
- output de ces tableaux
- filtrage de ces tableaux avec la méthode find() (API de ProcessWire)


## \_functions.php

Fonction | Description
------ | ----
getByClass() |
getById() |
getByTitle() |
buildClassArray() | sert à construire un tableau de données, il est filtrable selon la class souhaitée. On peut avoir la totalité des items la base comme seulement les items de class "book". On aboutit à un tableau d'id qu'il faut passer dans ```buildResourceArray()```
buildResourceArray() | sert à retrouver la valeur d'une propriété. Lorsqu'elle n'est pas un litteral mais une ressource, il y a une exception pour aller retrouver le title de la ressource en question
sendQuery() | Envoi une requête à la base de données. Possible de modifier le sélecteur, la table, les filtres, l'ordre et la direction de l'ordre
checkVariables() | Vérification que les noms des colonnes, que la table existe bien dans la base
getDatabaseColumns() | Retourne l'ensemble des noms des colonnes d'une table
checkAccess() | Vérification de la bonne connexion à la base de données
connectToDB() | Connexion à la base de données à partir des variables données dans le constructeur

## Détails des tables d'une base de données Omeka
- 'resource' : l'ensemble des items de la base avec la colonne 'resource_template_id' (ou 'resource_class_id'), on peut savoir de quel class est l'item
- 'property' : l'ensemble des propriétés de l'ensemble des vocabulaires installés dans la base
- 'value' : l'ensemble des valeurs ajoutées dans les champs des items, on peut retrouver l'item référent et l'id du champ / propriété

## TODO

- privilégier la propriété "dc:author" pour récupérer l'entièreté des auteurs
- fonction get() pour trouver un item par son id ou bien par son titre
- régler les choses quand plusieurs valeurs sont renseignées pour une même propriété
- convertir le tout en un module pour pouvoir l'appeler à la volée dans les templates
- créer des fonctions de tri, de filtre
- créer des fonctions pour récupérer les ressources où une ressource est liée
- créer des fonctions pour filtrer les code MARC Relators et créer un  hyperlien vers le maker s'il existe dans la base