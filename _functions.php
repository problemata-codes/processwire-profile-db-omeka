<?php

class OmekaItems {
	// variables de connexion
	private $host;
	private $username;
	private $password;
	private $name;
	private $charset;
	private $port;

	// variables de db
	private $db;

	public function __construct() {
		$this->host = "localhost";
		$this->username = "root";
		$this->password = "root";
		$this->name = "omeka_bibliotheque";
		$this->charset = "utf8";
		$this->port = "8889";

		// init la connexion à la db
		$this->db = $this->connectToDB();
	}

	public function connectToDB() {
		try	{
			$dsn = "mysql:dbname=$this->name;host=$this->host";
			if($this->port) $dsn .= ";port=$this->port";

			if (!$this->checkAccess()) {
				$this->host = 'localhost'; // we reset to localhost
				return false;
			}
			
			$driver_options = array(
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '$this->charset'",
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_TIMEOUT => 5
				);
			$database = new WireDatabasePDO($dsn, $this->username, $this->password, $driver_options); 
			$database->setDebugMode(wire('config')->debug);
			// print_r('Success');
			return $database;
		} catch(Exception $e) {
			if (!count($_POST)) {
				print_r('Setting an external database failed: '.$e->getMessage());
				exit();	
				return false;
			}
		}
	}
	
	public function checkAccess() {
		if(!function_exists('socket_create')) return true;
		// make all errors, warnings, notices exception
		// set_error_handler(function($errno, $errstr) {throw new Exception($errstr, $errno);});
		
		if (!filter_var($this->host, FILTER_VALIDATE_IP)) {
			$_host = gethostbyname($this->host);	
			if (empty($_POST) && $_host == $this->host) print_r("Unable to translate '$this->host' to valid IP Adress (IPv4)");
			else if (empty($_POST) && !filter_var($_host, FILTER_VALIDATE_IP)) print_r('Invalid IPv4 syntax for host');
			else $this->host = $_host;	
		}

		// if (!empty(print_r(array('array')))) return false;

		$socket = socket_create(AF_INET, SOCK_STREAM, 0);
		$message = '';
		try {
			if (socket_connect($socket, $this->host, $this->port)) {
				socket_close($socket);
				return true;
			}
		}
		catch (Exception $e) {
			$message = "Error: ".$e->getMessage();
		}
		if (empty($_POST)) print_r("No response from [$this->host:$this->port]. $message");
		restore_error_handler();
		return false;
	}
	
	public function getDatabaseColumns($table) {
		$db = $this->db;
		if(!$db||!in_array($table,$db->getTables())) return array();
		$columns = array();
		$table = $db->escapeTable($table);
		$sql = "SHOW COLUMNS FROM $table"; 			
		$query = $db->query($sql);
		if(!$query->rowCount()) return array();
		$rows = $query->fetchAll();
		foreach($rows as $row) {
			$columns[] = $row['Field'];
		}
		bd($columns, "COLUMNS");
		return $columns;
	}

	// vérif que les variables existent dans la db
	public function checkVariables($table, $col_val, $col_label) {
		$db = $this->db;

		// quick exit
		if (!$db) return false;

		// check if table exist
		// escapeTable = sanitize a table name for _a-zA-Z0-9
		$table = $db->escapeTable($table);
		// getTables = get array of all tables in this database.
		if(!in_array($table, $db->getTables())) return false;

		// get columns array
		$columns = $this->getDatabaseColumns($table);
		
		// check if column isset and exist, default 1st column
		if($col_label === false) $col_label = $columns[0];
		else if(!in_array($col_label,$columns)) return false;
 
		// check if column isset and exist, default 1st column of type int
		if($col_val === false) $col_val = $intcolumn;
		else if(!in_array($col_val,$columns)) return false;

		return true;
	}

	public function sendQuery($selector, $table, $filter, $col_label = false, $order = "id", $dir = false) {
		$db = $this->db;

		// direction de l'ordre des réponses
		$dir = ((bool)$dir) ? 'DESC' : 'ASC';
		
		// ordre des réponses
		$order = ($order) ? $order : $col_label;
		$order = $db->escapeCol($order); // escapeCol = sanitize a column name for _a-zA-Z0-9
		
		// validate/sanitize filterstring
		$filter = ($filter) ? "WHERE $filter" : '';
		
		// construction de la requête
		$query = $db->prepare("SELECT $selector FROM `$table` $filter ORDER BY `$order` $dir");
		$query->execute();
		if(!$query->rowCount()) return null;

		return $query;
	}


	// vars : $filter, $order, $dir
	public function buildClassArray($filter, $order, $dir) {

		// variables pour explorer la table ressource
		$selector = "*";
		$table = "resource";
		$col_val = "id";
		$col_label = "id";

		// vérif que les variables existent dans la db
		if (!$this->checkVariables($table, $col_val, $col_label)) return "Problèmes de variables";

		// envoi d'une requête mysql
		$query = $this->sendQuery($selector, $table, $filter, $col_label, $order, $dir);
		
		// ajout des id des lignes résultats de la requête au tableau $items_id
		$items_id = array();
	    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
	    	// bd($row[$col_val], "row[col_val]");
	    	$items_id[$row[$col_val]] = $row[$col_val];
	    }
	    // bd($items_id, "tableau de tous les items de la class");

	    $array = new WireArray();
	    foreach ($items_id as $key => $id) {
	    	// bd($id, "item id");

	    	// push d'un tableau (key => "value") de chaque ressource
	    	// dans un tableau pour la class entière
	    	$array->push($this->buildResourceArray($id));
	    }
	    // bd($array, "tableau des items de la class");
	    return $array;
	}

	public function buildResourceArray($id) {
		$selector = "*";
		$table = "value";
		$col_val = "resource_id";
		$col_label = "value";
		$filter = "`resource_id`=$id";
		$filter2 = "`property_id`='1'";
		// $filter = ($filter)?"WHERE $filter AND $filter2":'';

		$order = false;
		$dir = null;

		// vérif que les variables existent dans la db
		if (!$this->checkVariables($table, $col_val, $col_label)) return "Problèmes de variables";

		// envoi d'une requête mysql
		$query = $this->sendQuery($selector, $table, $filter);

		$properties_array = new WireArray();
		while($row = $query->fetch(PDO::FETCH_ASSOC)) {
			// bd($row['property_id'], "prop");

			// on cherche à créer des entrées "property_name" => "value"

			// à partir de l'id de la propriété on retrouve son name dans la table 'property'	
			$prop_id = $row['property_id'];

			$selector = "local_name";
			$table = "property";
			$filter = "`id`=$prop_id";
			$query_prop = $this->sendQuery($selector, $table, $filter);

			// $prop_name contient le nom de la propriété
			$prop_name = $query_prop->fetch()[0];
			
			// ajout de 'value' en face de la clé 'property'
			// avec exception si la 'value' est null
			// c'est le cas quand le champ est une ressource
			if ($row['value'] === null) {
				$res_id = $row['value_resource_id'];

				$selector = "value";
				$table = "value";
				$filter = "`resource_id`=$res_id AND `property_id`='1'";

				$query_resource = $this->sendQuery($selector, $table, $filter);
				$resource_name = $query_resource->fetch()[0];

				$properties_array[$prop_name] = $resource_name;
			} else {
				$properties_array[$prop_name] = $row['value'];
			}
		}
		// bd($properties_array, "properties_array");
		return $properties_array;
	}


	/* retourne une d'items à partir d'une class */
	public function getByClass($class) {
		$array = $this->buildClassArray("`resource_template_id`='$class'", false, false);
		return $array;
	}

	/* retourne le titre d'un item à partir de son id */
	public function getById($id) {
		/*
		$selector = "value";
		$table = "value";
		$filter = "resource_id='$id' AND `property_id`='1'";

		$query = $this->sendQuery($selector, $table, $filter);

		$resource= $query->fetch()[0];
		*/
		$resource = $this->buildResourceArray($id);
		return $resource;
	}

	public function getByTitle($str) {
		$selector = "resource_id";
		$table = "value";
		$filter = "`value`='$str' AND `property_id`='1'";

		$query = $this->sendQuery($selector, $table, $filter);
		$id = $query->fetch()[0];
		$resource = $this->buildResourceArray($id);
		return $resource;
	}
}

